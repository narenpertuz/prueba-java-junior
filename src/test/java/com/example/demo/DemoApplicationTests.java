package com.example.demo;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DemoApplicationTests {

	//Esta prueba unitaria aplica para evaluar los resultados cuya antigüedad de los empleados sea mayor o igual a 12 años

	@Test
	void testTotalSalarioPorDepartamento() {
		DemoApplication application = new DemoApplication();
		Map<String, Integer> response = application.totalSalarioPorDepartamento();
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("Control",9772295);
		map.put("Finanzas",5926870);
		map.put("Compras",11807626);
		map.put("Desarrollo",17376015);
		map.put("Gerencia",10430472);
		map.put("Marketing",33808995);
		assertEquals(map,response);
	}

}
