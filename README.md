**🚀Prueba práctica de Java perfil Junior🚀**


### Límite de tiempo
* 1 hora (60 minutos)

### Pre-requisitos
* Se necesita instalar Java Development Kit para compilar y correr la prueba (https://www.oracle.com/co/java/technologies/javase-downloads.html)

### Habilidades probadas
* Java, Manejo y procesamiento de colecciones(preferiblemente), realizar lectura de archivo plano(CSV)


### Propósito de la prueba
En la carpeta RESOURCES del proyecto se encuentra un archivo llamado empleados.csv, que contiene registros de los empleados de una empresa con la siguiente información:

* nombre: Nombre del empleado
* departamento: Departamento de la empresa en la cual el empleado labora
* salario: Salario del empleado
* antiguedad: Antigüedad en años del empleado en la empresa

El gerente general desea implementar una aplicación que le permita conocer cuanto dinero se destina a cada uno de los departamentos de la empresa de acuerdo al salario de sus
empleados. Además esta información debe ser filtrada según los años de antigüedad de las personas en la empresa.

* Ejemplo: Se cuenta con el siguiente set de datos y solo se desea considerar para el informe final a los empleados cuya antigüedad sea mayor o igual a 5 años

| nombre | departamento | salario | antigüedad|
|  ---   |     ---      |  ---    |   ---     | 
| empleado1 | Marketing | 2,100,000 | 2  |
| empleado2 | Comercial | 1,900,000 | 5  |
| empleado3 | Comercial | 1,900,000 | 7  |
| empleado4 | Logística | 1,700,000 | 1  |
| empleado5 | Logística | 2,500,000 | 6  |
| empleado6 | Logística | 2,500,000 | 7  |
| empleado7 | Financiero| 2,800,000 | 10 |
| empleado8 | Financiero| 2,100,000 | 6  |

* Informe de salida:

```json
{
  "Comercial": 3800000,
  "Logística": 5000000,
  "Financiero": 4900000
}
```



### Ejecución

El candidato debe implementar un método llamado "totalSalarioPorDepartamento" ubicado en com.example.demo.DemoApplication cuyo tipo de retorno debe ser 
Map<String, Integer>. Allí deberá realizar la lectura de los registros del archivo empleados.csv y a través de técnicas de procesamiento de datos 
que considere necesarias, debe agrupar y filtrar los registros para generar el tipo de salida requerido. En el mapa de retorno,
solo deben aparecer las sumatorias totales por departamento de los empleados cuya antigüedad sea mayor o igual a 12 años.

Nota: Se le solicita que NO modifique los registros del archivo empleados.csv y además tenga en cuenta que los valores mencionados en el propósito de la 
prueba se tratan simplemente de EJEMPLOS; por lo tanto se espera que con la lectura real del archivo empleados.csv se retornen sumatorias diferentes.










